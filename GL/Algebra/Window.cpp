#include <functional>
#include "Window.h"
#include <GL/glew.h>

void Window::InitWindow(int windowWidth, int windowHeight) {
	winHeight = windowHeight;
	winWidth = windowWidth;

	SDL_Init(SDL_INIT_VIDEO);

	window = SDL_CreateWindow
	("An SDL2 window", 10, 25, winWidth, winHeight, SDL_WINDOW_OPENGL);

	auto sdl_gl_context =
		SDL_GL_CreateContext(window);

	SDL_GL_SetAttribute(
		SDL_GL_CONTEXT_PROFILE_MASK,
		SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_GL_SetAttribute(
		SDL_GL_CONTEXT_MAJOR_VERSION, 4);

	SDL_GL_SetAttribute(
		SDL_GL_CONTEXT_MINOR_VERSION, 2);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);


	glGenTextures(1, &textureId);


	crtImage.img.assign(winWidth * winHeight, { 0.2, 0, 0.2, 1 });

	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexParameteri(GL_TEXTURE_2D,
		GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,
		GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

void Window::SetImage(const ImageData& image) {
	crtImage = image;
}

ImageData& Window::GetImageData() {
	return crtImage;
}

void Window::processWindow(std::function<bool(SDL_Event event)> processEvent, std::function<bool(ImageData&)> processImage) {


	bool loop = true;

	while (loop) {

		SDL_Event lastEvent{};
		SDL_Event event;

		SDL_PollEvent(&event);

		if (event.type != lastEvent.type) {
			if (!processEvent(event)) {
				loop = false;
			}

		}
		lastEvent = event;

		processImage(crtImage);

		glTexImage2D(GL_TEXTURE_2D,
			0, GL_RGBA, winWidth, winHeight,
			0, GL_RGBA, GL_FLOAT,
			&crtImage.img[0]);

		glClear(GL_COLOR_BUFFER_BIT);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, textureId);
		{
			glPushMatrix();
			glBegin(GL_QUADS);
			{
				glTexCoord2f(0, 0);
				glVertex2d(-1, 1);
				glTexCoord2f(1, 0);
				glVertex2d(1, 1);
				glTexCoord2f(1, 1);
				glVertex2d(1, -1);
				glTexCoord2f(0, 1);
				glVertex2d(-1, -1);
			}
			glEnd();
			glPopMatrix();

		}

		glDisable(GL_TEXTURE_2D);
		glFlush();

		SDL_GL_SwapWindow(window);
	}

}
