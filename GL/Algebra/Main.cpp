#define _USE_MATH_DEFINES
#include "Algebra.h"
#include "Mesh.h"
#include "Image.h"
#include "Window.h"
#include "Window.h"
#include "Tga.h"
#include "Camera.h"

#include <iostream>
#include <cmath>
#include <SDL.h>
#include <stdio.h>
#include <vector>
#include <string>
#include <GL/glew.h>
#include <sstream>



int main(int argc, char* argv[])
{
	Mesh mesh;

	mesh.ExtractObj(".\\cube.obj");

	// CAMERA
	algebra::Vec3<float> viewLookAt = { 0, 0, 1 };
	algebra::Vec3<float> viewUp = { 0, 1, 0 };
	algebra::Vec3<float> viewPosition = { 0, 0, -5};
	algebra::Vec3<float> viewTarget = viewPosition + viewLookAt;

	Camera cam{ viewPosition, viewTarget, viewUp };

	Window win;


	algebra::Matrix4<float> projection;
	projection = projection.projectionMatrix(win.windowHeight, win.windowWidth, 90 * M_PI / 180, 10000.0f, 0.1f);
	
	
	float val = 0;

	win.InitWindow(win.windowWidth, win.windowHeight);

	Tga texture;
	ImageData imgTexture;
	texture.load(imgTexture, ".\\logo.tga");
	
	std::cout << "Color: " << imgTexture.img[0] << "\n";
	win.processWindow([&val, &cam, &win](SDL_Event event)
	{
		switch (event.type) {
	
		case SDL_KEYDOWN:
			if (event.key.keysym.sym == SDLK_ESCAPE) {
				return false;
			}
			if (event.key.keysym.sym == SDLK_w) {
				auto increment = cam.target*0.1f;
				cam.position = cam.position -increment;
				
				cam.target = cam.target - increment;
				return true;
			}
			if (event.key.keysym.sym == SDLK_s) {
				auto increment = cam.target*0.1f;
				cam.position = cam.position + increment;
				cam.target = cam.target + increment;
				return true;
			}
			if (event.key.keysym.sym == SDLK_d) {
				val += 0.2f;
				algebra::Matrix3<float> rotateMatrix;
				auto rot = rotateMatrix.rotateY(val);
				cam.target = (cam.target - cam.position)*rot;

			}
			if (event.key.keysym.sym == SDLK_a) {
				val -= 0.2f;
				algebra::Matrix3<float> rotateMatrix;
				auto rot = rotateMatrix.rotateY(val);
				cam.target = (cam.target - cam.position)*rot;

			}
			break;
		}
		return true;
	},
		[&imgTexture, &win, &mesh, &projection, &cam](ImageData& img)
	{
		// Zbuffer vector with a size of the window
		std::vector<float> zBuffer(win.windowWidth*win.windowHeight);
		// Clear Zbuffer with infinity value

		std::fill(zBuffer.begin(), zBuffer.end(), INFINITY);
		
		std::fill(img.img.begin(), img.img.end(), algebra::Vec4<float>{ 0.2, 0, 0.2, 1 });
		int count=0;
		
		// Dessiner une image en haut � gauche
		for (algebra::Vec4<float> vec : imgTexture.img) {
			auto index = count % imgTexture.x;
			index += count / imgTexture.x * win.windowWidth;
			img.img[index] = vec;
			++count;
		}

		std::vector<algebra::Vec3<float>> points;

		static float timer=0;
		timer += 0.01f;
		algebra::Matrix3<float> rotationMatrix;



		for (const algebra::Vec3<float>& f : mesh.points) {
			algebra::Matrix3<float> mX;
			algebra::Matrix3<float> mY;
			algebra::Matrix3<float> mZ;

			mX = mX.rotateX(timer);
			mY = mY.rotateY(timer*0.7f);
			mZ = mZ.rotateZ(timer*1.2f);

			auto vec = f;
			rotationMatrix = mX * mY * mZ;
			vec = vec * mX * mY * mZ;
			
			points.push_back(vec);
		}

		auto point2 = points;


		algebra::Matrix4<float> matrixView = cam.View().inverse();

		std::vector<algebra::Vec3<float>> viewPoints = mesh.view(points, matrixView);

		std::vector<algebra::Vec4<float>> projectedPoints = mesh.project(viewPoints, win.windowWidth, win.windowHeight);
		
		for (auto& v : projectedPoints) {
			v = (v + 1) / 2;

			v.x *= win.windowWidth;
			v.y *= win.windowHeight;
		}

		algebra::Vec3<float> normalVertex1;
		algebra::Vec3<float> normalVertex2;
		algebra::Vec3<float> normalVertex3;

		for (int i = 0; i < mesh.pointsIndex.size(); i += 3) {
			if (mesh.normals.empty()) {
				algebra::Vec3<float> ABVector = point2[mesh.pointsIndex[i + 1]] - point2[mesh.pointsIndex[i]];
				algebra::Vec3<float> ACVector = point2[mesh.pointsIndex[i + 2]] - point2[mesh.pointsIndex[i]];
				
				auto normal = ABVector ^ ACVector;
				normal = normal.normalize();
				
				normalVertex1 = normal;
				normalVertex2 = normal;
				normalVertex3 = normal;
			}
			else {
				normalVertex1 = mesh.normals[mesh.normalsIndex[  i	]] * rotationMatrix;
				normalVertex2 = mesh.normals[mesh.normalsIndex[i + 1]] * rotationMatrix;
				normalVertex3 = mesh.normals[mesh.normalsIndex[i + 2]] * rotationMatrix;
			}

			
			win.GetImageData().FillTriangle(
				imgTexture,
				mesh,
				win.windowWidth,
				zBuffer,
				{  projectedPoints[mesh.pointsIndex[  i	 ]], img.ColorVertexA, normalVertex1, mesh.UVs[mesh.uvIndex[  i	 ]] },
				{  projectedPoints[mesh.pointsIndex[i + 1]], img.ColorVertexB, normalVertex2, mesh.UVs[mesh.uvIndex[i + 1]] },
				{  projectedPoints[mesh.pointsIndex[i + 2]], img.ColorVertexC, normalVertex3, mesh.UVs[mesh.uvIndex[i + 2]] }
			);
		}

		return true;
	});

	return 0;
}