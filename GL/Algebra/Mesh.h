#pragma once
#include <vector>
#include "Algebra.h"


class Mesh {
// variables
public:	
	std::vector<algebra::Vec3<float>> points;
	std::vector<algebra::Vec3<float>> normals;
	std::vector<algebra::Vec2<float>> UVs;
	std::vector<int> normalsIndex;
	std::vector<int> uvIndex;
	std::vector<int> pointsIndex;
// methods
public:
	std::vector<algebra::Vec4<float>> project(const std::vector<algebra::Vec3<float>>& points, int w, int h)const;
	std::vector<algebra::Vec3<float>> view(const std::vector<algebra::Vec3<float>>& points,const algebra::Matrix4<float>& view)const;
	bool ExtractObj(const std::string& path);
};