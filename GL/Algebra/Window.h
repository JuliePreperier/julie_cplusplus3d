#pragma once

#include "SDL.h"
#include "Image.h"
#include <functional>

#include <GL/glew.h>

struct Window {
public:
	int windowWidth = 640;
	int windowHeight = 480;
	void InitWindow(int windowWidth, int windowHeight);
	void SetImage(const ImageData& image);
	ImageData& GetImageData();
	void processWindow(std::function<bool(SDL_Event event)> processEvent, std::function<bool(ImageData&)> processImage);
private:
	SDL_Window* window;
	GLuint textureId = 0;
	int winWidth;
	int winHeight;
	ImageData crtImage;
};