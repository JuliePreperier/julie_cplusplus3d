#pragma once

#include "Image.h"

class Tga : public ImageInterface
{
public:
	bool save(
		const ImageData& data,
		const std::string& path) override;

	bool load(
		ImageData& data,
		const std::string& path) override;

private:
	size_t size;
};

