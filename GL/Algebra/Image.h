#pragma once

#include <vector>
#include <string>
#include "Algebra.h"
#include "Mesh.h"
#include "Image.h"

struct Vertex {
	algebra::Vec4<float> screenPosition;
	algebra::Vec4<float> color;
	algebra::Vec3<float> normal;
	algebra::Vec2<float> uv;

};

//technically you don't need 'y' because the vector give the size.
struct ImageData {
public:
	algebra::Vec4<float> ColorVertexA{ 1, 0, 0, 1 }; // RED
	algebra::Vec4<float> ColorVertexB{ 0, 1, 0, 1 }; // GREEN
	algebra::Vec4<float> ColorVertexC{ 0, 0, 1, 1 }; // BLUE

	size_t x;
	size_t y;
	std::vector<algebra::Vec4<float>> img;

	int x1;
	int y1;

	void drawPixel(int x, int y, algebra::Vec4<float> pixel);

	void drawLine(int x1, int y1, int x2, int y2, algebra::Vec4<float> startColorPix, algebra::Vec4<float> endColorPix);

	algebra::Vec4<float> FindSquare(Vertex a, Vertex b, Vertex c);

	void FindTopVertex(Vertex a, Vertex b, Vertex c);

	bool isInside(const float& s, const float& t) const;


	void FillTriangle(const ImageData& imgTexture, const Mesh& mesh, const float& windowWidth, std::vector<float>& zBuffer, const Vertex& a, const Vertex& b, const Vertex& c);

	float BarycentricS(const Vertex& a, const Vertex& b, const Vertex& c, const  algebra::Vec3<float>& p) const;
	float BarycentricT(const Vertex& a, const Vertex& b, const Vertex& c, const  algebra::Vec3<float>& p) const;
};

// & -> reference vers une donn�e
// const -> peuvent pas etre modifi�
struct ImageInterface {
	virtual bool save(
		const ImageData& data,
		const std::string& path) = 0;
	virtual bool load(
		ImageData& data,
		const std::string& path) = 0;

};
