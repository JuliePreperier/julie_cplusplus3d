#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include <cmath>
#include <iostream>

namespace algebra {
	template<typename T>
	class Vec2 {
	public:
		T x;
		T y;

		Vec2() {}

		Vec2(T var1, T var2) {
			x = var1;
			y = var2;
		}

		bool operator== (const Vec2<T>& v) const {
			return x == v.x && y == v.y;
		}

		bool operator> (const Vec2<T>& v) const {
			return double_length() > v.double_length();
		}

		bool operator< (const Vec2<T>& v) const {
			return double_length() < v.double_length();
		}

		bool operator>= (const Vec2<T>& v) const {
			return double_length() >= v.double_length();
		}

		bool operator<= (const Vec2<T>& v) const {
			return double_length() <= v.double_length();
		}

		T length() const {
			return std::sqrt(double_length());
		}

		T double_length() const {
			return x * x + y * y;
		}

		friend std::ostream& operator<< (std::ostream& os, const Vec2<T>& vec) {
			os << vec.x << ", " << vec.y;

			return os;
		}

		Vec2<T> normalize() const {
			Vec2<T> n(x, y);
			const T len = length();
			n.x /= len;
			n.y /= len;
			return n;
		}

		Vec2<T> operator= (const Vec2<T>& v) {
			x = v.x;
			y = v.y;

			return *this;
		}

		Vec2<T> operator+= (T s) {
			x += s;
			y += s;

			return *this;
		}

		Vec2<T> operator-= (T s) {
			x -= s;
			y -= s;

			return *this;
		}

		Vec2<T> operator*= (T s) {
			x *= s;
			y *= s;

			return *this;
		}

		Vec2<T> operator/= (T s) {
			x /= s;
			y /= s;

			return *this;
		}

		Vec2<T> operator+ (T s) const {
			Vec2<T> v2(x, y);
			v2 += s;
			return v2;
		}

		Vec2<T> operator- (T s) const {
			Vec2<T> v2(x, y);
			v2 -= s;
			return v2;
		}

		Vec2<T> operator* (T s) const {
			Vec2<T> v2(x, y);
			v2 *= s;
			return v2;
		}

		Vec2<T> operator/ (T s) const {
			Vec2<T> v2(x, y);
			v2 /= s;
			return v2;
		}

		Vec2<T> operator+ (const Vec2<T>& v) const {
			Vec2<T> v2(x, y);
			v2.x += v.x;
			v2.y += v.y;

			return v2;
		}

		Vec2<T> operator- (const Vec2<T>& v) const {
			Vec2<T> v2(x, y);
			v2.x -= v.x;
			v2.y -= v.y;

			return v2;
		}

		T operator* (const Vec2<T>& v) const {
			return v.x * x + v.y * y;
		}

	};

	template<typename T>
	class Vec3 {
	public:
		T x;
		T y;
		T z;

		Vec3() {}

		Vec3(T var1, T var2, T var3) {
			x = var1;
			y = var2;
			z = var3;
		}

		bool operator== (const Vec3<T>& v) const {
			return x == v.x && y == v.y && z == v.z;
		}

		bool operator> (const Vec3<T>& v) const {
			return double_length() > v.double_length();
		}

		bool operator< (const Vec3<T>& v) const {
			return double_length() < v.double_length();
		}

		bool operator>= (const Vec3<T>& v) const {
			return double_length() >= v.double_length();
		}

		bool operator<= (const Vec3<T>& v) const {
			return double_length() <= v.double_length();
		}

		T length() const {
			return std::sqrt(double_length());
		}

		T double_length() const {
			return x * x + y * y + z * z;
		}

		friend std::ostream& operator<< (std::ostream& os, const Vec3<T> vec) {
			os << vec.x << ", " << vec.y << ", " << vec.z;

			return os;
		}

		Vec3<T> normalize() const {
			Vec3<T> n(x, y, z);
			const T len = length();
			n.x /= len;
			n.y /= len;
			n.z /= len;
			return n;
		}

		Vec3<T> operator= (const Vec3<T>& v) {
			x = v.x;
			y = v.y;
			z = v.z;

			return *this;
		}

		Vec3<T> operator+= (T s) {
			x += s;
			y += s;
			z += s;

			return *this;
		}

		Vec3<T> operator-= (T s) {
			x -= s;
			y -= s;
			z -= s;

			return *this;
		}

		Vec3<T> operator*= (T s) {
			x *= s;
			y *= s;
			z *= s;

			return *this;
		}

		Vec3<T> operator/= (T s) {
			x /= s;
			y /= s;
			z /= s;

			return *this;
		}

		Vec3<T> operator+ (T s) const {
			Vec3<T> v3(x, y, z);
			v3 += s;
			return v3;
		}

		Vec3<T> operator- (T s) const {
			Vec3<T> v3(x, y, z);
			v3 -= s;
			return v3;
		}

		Vec3<T> operator* (T s) const {
			Vec3<T> v3(x, y, z);
			v3 *= s;
			return v3;
		}

		Vec3<T> operator/ (T s) const {
			Vec3<T> v3(x, y, z);
			v3 /= s;
			return v3;
		}

		Vec3<T> operator+ (const Vec3<T>& v) const {
			Vec3<T> v3(x, y, z);
			v3.x += v.x;
			v3.y += v.y;
			v3.z += v.z;

			return v3;
		}

		Vec3<T> operator- (const Vec3<T>& v) const {
			Vec3<T> v3(x, y, z);
			v3.x -= v.x;
			v3.y -= v.y;
			v3.z -= v.z;

			return v3;
		}

		T operator* (const Vec3<T>& v) const {
			return v.x * x + v.y * y + v.z * z;
		}

		Vec3<T> operator^ (const Vec3<T>& v) const {
			return Vec3<T>(
				y * v.z - z * v.y,
				z * v.x - x * v.z,
				x * v.y - y * v.x);
		}
	};

	template<typename T>
	class Vec4 {
	public:
		T x;
		T y;
		T z;
		T w;

		Vec4() {}

		Vec4(T var1, T var2, T var3, T var4) {
			x = var1;
			y = var2;
			z = var3;
			w = var4;
		}

		Vec4(Vec3<T> vec3, T var4) {
			x = vec3.x;
			y = vec3.y;
			z = vec3.z;
			w = var4;
		}

		friend std::ostream& operator<< (std::ostream& os, const Vec4<T> vec) {
			os << vec.x << ", " << vec.y << ", " << vec.z << ", " << vec.w;

			return os;
		}

		bool operator== (const Vec4<T>& v) const {
			return x == v.x && y == v.y && z == v.z && w == v.w;
		}

		bool operator> (const Vec4<T>& v) const {
			return double_length() > v.double_length();
		}

		bool operator< (const Vec4<T>& v) const {
			return double_length() < v.double_length();
		}

		bool operator>= (const Vec4<T>& v) const {
			return double_length() >= v.double_length();
		}

		bool operator<= (const Vec4<T>& v) const {
			return double_length() <= v.double_length();
		}

		T length() const {
			return std::sqrt(double_length());
		}

		T double_length() const {
			return x * x + y * y + z * z + w * w;
		}

		Vec4<T> normalize() const {
			Vec4<T> n(x, y, z, w);
			const T len = length();
			n.x /= len;
			n.y /= len;
			n.z /= len;
			n.w /= len;
			return n;
		}

		Vec4<T> operator= (const Vec4<T>& v) {
			x = v.x;
			y = v.y;
			z = v.z;
			w = v.w;

			return *this;
		}

		Vec4<T> operator+= (T s) {
			x += s;
			y += s;
			z += s;
			w += s;

			return *this;
		}

		Vec4<T> operator-= (T s) {
			x -= s;
			y -= s;
			z -= s;
			w -= s;

			return *this;
		}

		Vec4<T> operator*= (T s) {
			x *= s;
			y *= s;
			z *= s;
			w *= s;

			return *this;
		}

		Vec4<T> operator/= (T s) {
			x /= s;
			y /= s;
			z /= s;
			w /= s;

			return *this;
		}

		Vec4<T> operator+ (T s) const {
			Vec4<T> v4(x, y, z, w);
			v4 += s;
			return v4;
		}

		Vec4<T> operator- (T s) const {
			Vec4<T> v4(x, y, z, w);
			v4 -= s;
			return v4;
		}

		Vec4<T> operator* (T s) const {
			Vec4<T> v4(x, y, z, w);
			v4 *= s;
			return v4;
		}

		Vec4<T> operator/ (T s) const {
			Vec4<T> v4(x, y, z, w);
			v4 /= s;
			return v4;
		}

		Vec4<T> operator+ (const Vec4<T>& v) const {
			Vec4<T> v4(x, y, z, w);
			v4.x += v.x;
			v4.y += v.y;
			v4.z += v.z;
			v4.w += v.w;

			return v4;
		}

		Vec4<T> operator- (const Vec4<T>& v) const {
			Vec4<T> v4(x, y, z, w);
			v4.x -= v.x;
			v4.y -= v.y;
			v4.z -= v.z;
			v4.w -= v.w;

			return v4;
		}

		T operator* (const Vec4<T>& v) const {
			return v.x * x + v.y * y + v.z * z + v.w * w;
		}

		Vec4<T> operator| (const Vec4<T>& v)const {
			Vec4<T> v4(x, y, z, w);
			v4.x = x * v.x;
			v4.y = y * v.y;
			v4.z = z * v.z;
			v4.w = w * v.w;

			return v4;
		}

	};

	template<typename T>
	class Matrix2 {
	public:
		Vec2<T> vec1;
		Vec2<T> vec2;

		Matrix2() {}

		Matrix2(const Matrix2<T>& m) {
			vec1 = m.vec1;
			vec2 = m.vec2;
		}

		Matrix2(
			T v11, T v12,
			T v21, T v22) :
			vec1(v11, v12),
			vec2(v21, v22) {}

		friend std::ostream& operator<< (std::ostream& os, const Matrix2<T>& mat) {
			os << mat.vec1 << "\n";
			os << mat.vec2 << "\n";

			return os;
		}

		bool operator== (const Matrix2<T>& m) const {
			return m.vec1 == vec1 && m.vec2 == vec2;
		}

		Matrix2<T> identity() {
			return Matrix2(1, 0, 0, 1);
		}

		Matrix2<T> operator+ (const Matrix2<T>& m) const {
			Matrix2<T> mat(*this);

			mat.vec1 += m.vec1;
			mat.vec2 += m.vec2;

			return mat;
		}

		Matrix2<T> operator* (T s) const {
			Matrix2<T> mat(*this);

			mat.vec1 *= s;
			mat.vec2 *= s;

			return mat;
		}

		Matrix2<T> trans() const {
			Matrix2<T> mat;

			mat.vec1.x = vec1.x;
			mat.vec1.y = vec2.x;

			mat.vec2.x = vec1.y;
			mat.vec2.y = vec2.y;

			return mat;
		}

		Matrix2<T> operator* (const Matrix2<T> m) const {
			Matrix2<T> mOut;
			Matrix2<T> mTrans = m.trans();

			mOut.vec1.x = vec1 * mTrans.vec1;
			mOut.vec1.y = vec1 * mTrans.vec2;

			mOut.vec2.x = vec2 * mTrans.vec1;
			mOut.vec2.y = vec2 * mTrans.vec2;

			return mOut;
		}

		T det() const {
			return (vec1.x * vec2.y) - (vec1.y*vec2.x);
		}

		Matrix2<T> adj() const {
			Matrix2<T> minor(vec2.y, vec2.x, vec1.y, vec1.x);
			minor.vec1.y *= -1;
			minor.vec2.x *= -1;
			minor = minor.trans();
			return minor;
		}

		Matrix2<T> inverse() const {
			return adj() * (1.0f / det());
		}
	};

	template<typename T>
	class Matrix3 {
	public:
		Vec3<T> vec1;
		Vec3<T> vec2;
		Vec3<T> vec3;

		Matrix3() {}

		Matrix3(const Matrix3<T>& m) {
			vec1 = m.vec1;
			vec2 = m.vec2;
			vec3 = m.vec3;
		}

		Matrix3(
			T v11, T v12, T v13,
			T v21, T v22, T v23,
			T v31, T v32, T v33) :
			vec1(v11, v12, v13),
			vec2(v21, v22, v23),
			vec3(v31, v32, v33) {}

		bool operator== (const Matrix3<T>& m) const {
			return m.vec1 == vec1 && m.vec2 == vec2 && m.vec3 == vec3;
		}

		Matrix3<T> identity() {
			return Matrix3(
				1, 0, 0,
				0, 1, 0,
				0, 0, 1);
		}

		Matrix3<T> operator+ (const Matrix3<T>& m) const {
			Matrix3<T> mat(*this);

			mat.vec1 += m.vec1;
			mat.vec2 += m.vec2;
			mat.vec3 += m.vec3;

			return mat;
		}

		Matrix3<T> operator* (T s) const {
			Matrix3<T> mat(*this);

			mat.vec1 *= s;
			mat.vec2 *= s;
			mat.vec3 *= s;

			return mat;
		}

		Matrix3<T> trans() const {
			Matrix3<T> mat;

			mat.vec1.x = vec1.x;
			mat.vec1.y = vec2.x;
			mat.vec1.z = vec3.x;

			mat.vec2.x = vec1.y;
			mat.vec2.y = vec2.y;
			mat.vec2.z = vec3.y;

			mat.vec3.x = vec1.z;
			mat.vec3.y = vec2.z;
			mat.vec3.z = vec3.z;

			return mat;
		}

		Matrix3<T> operator* (const Matrix3<T> m) const {
			Matrix3<T> mOut;
			Matrix3<T> mTrans = m.trans();

			mOut.vec1.x = vec1 * mTrans.vec1;
			mOut.vec1.y = vec1 * mTrans.vec2;
			mOut.vec1.z = vec1 * mTrans.vec3;

			mOut.vec2.x = vec2 * mTrans.vec1;
			mOut.vec2.y = vec2 * mTrans.vec2;
			mOut.vec2.z = vec2 * mTrans.vec3;

			mOut.vec3.x = vec3 * mTrans.vec1;
			mOut.vec3.y = vec3 * mTrans.vec2;
			mOut.vec3.z = vec3 * mTrans.vec3;

			return mOut;
		}

		friend std::ostream& operator<< (std::ostream& os, const Matrix3<T>& mat) {
			os << mat.vec1 << "\n";
			os << mat.vec2 << "\n";
			os << mat.vec3 << "\n";

			return os;
		}

		T det() const {
			Matrix2<T> matefhi(vec2.y, vec2.z, vec3.y, vec3.z);
			Matrix2<T> matdfgi(vec2.x, vec2.z, vec3.x, vec3.z);
			Matrix2<T> matdegh(vec2.x, vec2.y, vec3.x, vec3.y);

			return (vec1.x*matefhi.det()) - (vec1.y*matdfgi.det()) + (vec1.z*matdegh.det());
		}

		Matrix3<T> adj() const {
			Matrix2<T> efhi(vec2.y, vec2.z, vec3.y, vec3.z);
			Matrix2<T> dfgi(vec2.x, vec2.z, vec3.x, vec3.z);
			Matrix2<T> degh(vec2.x, vec2.y, vec3.x, vec3.y);

			Matrix2<T> bchi(vec1.y, vec1.z, vec3.y, vec3.z);
			Matrix2<T> acgi(vec1.x, vec1.z, vec3.x, vec3.z);
			Matrix2<T> abgh(vec1.x, vec1.y, vec3.x, vec3.y);

			Matrix2<T> bcef(vec1.y, vec1.z, vec2.y, vec2.z);
			Matrix2<T> acdf(vec1.x, vec1.z, vec2.x, vec2.z);
			Matrix2<T> abde(vec1.x, vec1.y, vec2.x, vec2.y);

			Matrix3<T> minor(
				efhi.det(), dfgi.det(), degh.det(),
				bchi.det(), acgi.det(), abgh.det(),
				bcef.det(), acdf.det(), abde.det());

			minor.vec1.y *= -1;
			minor.vec2.x *= -1;
			minor.vec2.z *= -1;
			minor.vec3.y *= -1;

			minor = minor.trans();
			return minor;
		}

		Matrix3<T> rotateX(T angle) const {
			Matrix3<T> mOut{
				1,0,0,
				0,std::cos(angle), -std::sin(angle),
				0,std::sin(angle),	std::cos(angle)
			};

			return mOut;
		}

		Matrix3<T> rotateY(T angle) const {
			Matrix3<T> mOut{
				std::cos(angle), 0, std::sin(angle),
				0, 1, 0,
				-std::sin(angle), 0, std::cos(angle)
			};

			return mOut;
		}

		Matrix3<T> rotateZ(T angle) const {
			Matrix3<T> mOut{
				std::cos(angle), -std::sin(angle), 0,
				std::sin(angle), std::cos(angle), 0,
				0, 0, 1
			};

			return mOut;
		}

		Matrix3<T> inverse() const {
			return adj() * (1.0f / det());
		}
	};

	template<typename T>
	class Matrix4 {
	public:
		Vec4<T> vec1;
		Vec4<T> vec2;
		Vec4<T> vec3;
		Vec4<T> vec4;

		Matrix4() {}

		Matrix4(const Matrix4<T>& m) {
			vec1 = m.vec1;
			vec2 = m.vec2;
			vec3 = m.vec3;
			vec4 = m.vec4;
		}

		Matrix4(
			T v11, T v12, T v13, T v14,
			T v21, T v22, T v23, T v24,
			T v31, T v32, T v33, T v34,
			T v41, T v42, T v43, T v44) :
			vec1(v11, v12, v13, v14),
			vec2(v21, v22, v23, v24),
			vec3(v31, v32, v33, v34),
			vec4(v41, v42, v43, v44) {	}

		bool operator== (const Matrix4<T>& m) const {
			return m.vec1 == vec1 && m.vec2 == vec2 && m.vec3 == vec3 && m.vec4 == vec4;
		}

		friend std::ostream& operator<< (std::ostream& os, const Matrix4<T>& mat) {
			os << mat.vec1 << "\n";
			os << mat.vec2 << "\n";
			os << mat.vec3 << "\n";
			os << mat.vec4 << "\n";

			return os;
		}

		Matrix4<T> identity() {
			return Matrix4(
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				0, 0, 0, 1);
		}

		Matrix4<T> operator+ (const Matrix4<T>& m) const {
			Matrix4<T> mat(*this);

			mat.vec1 += m.vec1;
			mat.vec2 += m.vec2;
			mat.vec3 += m.vec3;
			mat.vec4 += m.vec4;

			return mat;
		}

		Matrix4<T> operator* (T s) const {
			Matrix4<T> mat(*this);

			mat.vec1 *= s;
			mat.vec2 *= s;
			mat.vec3 *= s;
			mat.vec4 *= s;

			return mat;
		}

		Matrix4<T> trans() const {
			Matrix4<T> mat;

			mat.vec1.x = vec1.x;
			mat.vec1.y = vec2.x;
			mat.vec1.z = vec3.x;
			mat.vec1.w = vec4.x;

			mat.vec2.x = vec1.y;
			mat.vec2.y = vec2.y;
			mat.vec2.z = vec3.y;
			mat.vec2.w = vec4.y;

			mat.vec3.x = vec1.z;
			mat.vec3.y = vec2.z;
			mat.vec3.z = vec3.z;
			mat.vec3.w = vec4.z;

			mat.vec4.x = vec1.w;
			mat.vec4.y = vec2.w;
			mat.vec4.z = vec3.w;
			mat.vec4.w = vec4.w;

			return mat;
		}

		Matrix4<T> operator* (const Matrix4<T>& m) const {
			Matrix4<T> mOut;
			Matrix4<T> mTrans = m.trans();

			mOut.vec1.x = vec1 * mTrans.vec1;
			mOut.vec1.y = vec1 * mTrans.vec2;
			mOut.vec1.z = vec1 * mTrans.vec3;
			mOut.vec1.w = vec1 * mTrans.vec4;

			mOut.vec2.x = vec2 * mTrans.vec1;
			mOut.vec2.y = vec2 * mTrans.vec2;
			mOut.vec2.z = vec2 * mTrans.vec3;
			mOut.vec2.w = vec2 * mTrans.vec4;

			mOut.vec3.x = vec3 * mTrans.vec1;
			mOut.vec3.y = vec3 * mTrans.vec2;
			mOut.vec3.z = vec3 * mTrans.vec3;
			mOut.vec3.w = vec3 * mTrans.vec4;

			mOut.vec4.x = vec4 * mTrans.vec1;
			mOut.vec4.y = vec4 * mTrans.vec2;
			mOut.vec4.z = vec4 * mTrans.vec3;
			mOut.vec4.w = vec4 * mTrans.vec4;

			return mOut;
		}

		T det() const {
			Matrix3<T> matfghjklnop(
				vec2.y, vec2.z, vec2.w,
				vec3.y, vec3.z, vec3.w,
				vec4.y, vec4.z, vec4.w);
			Matrix3<T> mateghiklmop(
				vec2.x, vec2.z, vec2.w,
				vec3.x, vec3.z, vec3.w,
				vec4.x, vec4.z, vec4.w);
			Matrix3<T> matefhijlmnp(
				vec2.x, vec2.y, vec2.w,
				vec3.x, vec3.y, vec3.w,
				vec4.x, vec4.y, vec4.w);
			Matrix3<T> matefgijkmno(
				vec2.x, vec2.y, vec2.z,
				vec3.x, vec3.y, vec3.z,
				vec4.x, vec4.y, vec4.z);

			return (vec1.x*matfghjklnop.det()) - (vec1.y*mateghiklmop.det()) + (vec1.z*matefhijlmnp.det()) - (vec1.w*matefgijkmno.det());
		}

		Matrix4<T> adj() const {
			// Ligne 1
			Matrix3<T> fghjklnop(
				vec2.y, vec2.z, vec2.w,
				vec3.y, vec3.z, vec3.w,
				vec4.y, vec4.z, vec4.w);
			Matrix3<T> eghiklmop(
				vec2.x, vec2.z, vec2.w,
				vec3.x, vec3.z, vec3.w,
				vec4.x, vec4.z, vec4.w);
			Matrix3<T> efhijlmnp(
				vec2.x, vec2.y, vec2.w,
				vec3.x, vec3.y, vec3.w,
				vec4.x, vec4.y, vec4.w);
			Matrix3<T> efgijkmno(
				vec2.x, vec2.y, vec2.z,
				vec3.x, vec3.y, vec3.z,
				vec4.x, vec4.y, vec4.z);
			// ligne 2
			Matrix3<T> bcdjklnop(
				vec1.y, vec1.z, vec1.w,
				vec3.y, vec3.z, vec3.w,
				vec4.y, vec4.z, vec4.w);
			Matrix3<T> acdiklmop(
				vec1.x, vec1.z, vec1.w,
				vec3.x, vec3.z, vec3.w,
				vec4.x, vec4.z, vec4.w);
			Matrix3<T> abdijlmnp(
				vec1.x, vec1.y, vec1.w,
				vec3.x, vec3.y, vec3.w,
				vec4.x, vec4.y, vec4.w);
			Matrix3<T> abcijkmno(
				vec1.x, vec1.y, vec1.z,
				vec3.x, vec3.y, vec3.z,
				vec4.x, vec4.y, vec4.z);
			// ligne 3
			Matrix3<T> bcdfghnop(
				vec1.y, vec1.z, vec1.w,
				vec2.y, vec2.z, vec2.w,
				vec4.y, vec4.z, vec4.w);
			Matrix3<T> acdeghmop(
				vec1.x, vec1.z, vec1.w,
				vec2.x, vec2.z, vec2.w,
				vec4.x, vec4.z, vec4.w);
			Matrix3<T> abdefhmnp(
				vec1.x, vec1.y, vec1.w,
				vec2.x, vec2.y, vec2.w,
				vec4.x, vec4.y, vec4.w);
			Matrix3<T> abcefgmno(
				vec1.x, vec1.y, vec1.z,
				vec2.x, vec2.y, vec2.z,
				vec4.x, vec4.y, vec4.z);
			// ligne 4
			Matrix3<T> bcdfghjkl(
				vec1.y, vec1.z, vec1.w,
				vec2.y, vec2.z, vec2.w,
				vec3.y, vec3.z, vec3.w);
			Matrix3<T> acdeghikl(
				vec1.x, vec1.z, vec1.w,
				vec2.x, vec2.z, vec2.w,
				vec3.x, vec3.z, vec3.w);
			Matrix3<T> abdefhijl(
				vec1.x, vec1.y, vec1.w,
				vec2.x, vec2.y, vec2.w,
				vec3.x, vec3.y, vec3.w);
			Matrix3<T> abcefgijk(
				vec1.x, vec1.y, vec1.z,
				vec2.x, vec2.y, vec2.z,
				vec3.x, vec3.y, vec3.z);


			Matrix4<T> minor(
				fghjklnop.det(), eghiklmop.det(), efhijlmnp.det(), efgijkmno.det(),
				bcdjklnop.det(), acdiklmop.det(), abdijlmnp.det(), abcijkmno.det(),
				bcdfghnop.det(), acdeghmop.det(), abdefhmnp.det(), abcefgmno.det(),
				bcdfghjkl.det(), acdeghikl.det(), abdefhijl.det(), abcefgijk.det());

			minor.vec1.y *= -1;
			minor.vec1.w *= -1;

			minor.vec2.x *= -1;
			minor.vec2.z *= -1;

			minor.vec3.y *= -1;
			minor.vec3.w *= -1;

			minor.vec4.x *= -1;
			minor.vec4.z *= -1;

			minor = minor.trans();
			return minor;
		}

		Matrix4<T> inverse() const {
			return adj() * (1.0f / det());
		}

		Matrix4<T> projectionMatrix(T height, T width, T theta, T ZFar, T ZNear) {
			T mat0_0 = (height / width) * 1 / std::tan(theta / 2);
			T mat1_1 = 1 / std::tan(theta / 2);
			T mat2_2 = ZFar / (ZFar - ZNear);
			T mat3_2 = -ZFar * ZNear / (ZFar - ZNear);
			
			Matrix4<T> projection = {
				mat0_0,0,0,0,
				0,mat1_1,0,0,
				0,0,mat2_2,1,
				0,0,mat3_2,0
			};

			return projection;
		}




	};

	template<typename T>
	class Quaternion {
	public:
		T x;
		T y;
		T z;
		T w;
		const float epsilon = 1e-5;

		Quaternion() {}

		Quaternion(T var1, T var2, T var3, T var4) {
			x = var1;
			y = var2;
			z = var3;
			w = var4;
		}

		Quaternion<T> operator+ (const Quaternion<T>& q)const {
			return Quaternion<T>(x + q.x, y + q.y, z + q.z, w + q.w);
		}

		Quaternion<T> operator* (const Quaternion<T>& q)const {
			return Quaternion<T>(
				x*q.x - y * q.y - z * q.z - w * q.w,
				x*q.y + y * q.x + z * q.w - w * q.z,
				x*q.z - y * q.w + z * q.x + w * q.y,
				x*q.w + y * q.z - z * q.y + w * q.x);
		}

		Quaternion<T> ToQuaternion(float yaw, float pitch, float roll) // yaw (Z), pitch (Y), roll (X)
		{
			// Abbreviations for the various angular functions
			float cy = cos(yaw * 0.5);
			float sy = sin(yaw * 0.5);
			float cp = cos(pitch * 0.5);
			float sp = sin(pitch * 0.5);
			float cr = cos(roll * 0.5);
			float sr = sin(roll * 0.5);

			Quaternion q;
			q.x = cy * cp * cr + sy * sp * sr;
			q.y = cy * cp * sr - sy * sp * cr;
			q.z = sy * cp * sr + cy * sp * cr;
			q.w = sy * cp * cr - cy * sp * sr;

			return q;
		}

		Vec3<T> ToEulerAngles(Quaternion q)
		{
			Vec3 angles;

			// roll (x-axis rotation)
			float sinr_cosp = +2.0 * (q.x * q.y + q.z * q.w);
			float cosr_cosp = +1.0 - 2.0 * (q.y * q.y + q.z * q.z);
			angles.x = atan2(sinr_cosp, cosr_cosp);

			// pitch (y-axis rotation)
			float sinp = +2.0 * (q.x * q.z - q.w * q.y);
			if (fabs(sinp) >= 1)
				angles.y = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
			else
				angles.y = asin(sinp);

			// yaw (z-axis rotation)
			float siny_cosp = +2.0 * (q.x * q.w + q.y * q.z);
			float cosy_cosp = +1.0 - 2.0 * (q.z * q.z + q.w * q.w);

			angles.z = atan2(siny_cosp, cosy_cosp);

			return angles;
		}

		bool operator== (const Quaternion<T>& q) const {
			return x == q.x && y == q.y && z == q.z && w == q.w;
		}

		Quaternion<T> operator= (const Quaternion<T>& q) {
			x = q.x;
			y = q.y;
			z = q.z;
			w = q.w;

			return *this;
		}

		/*bool operator== <float>(const Quaternion<float>& q)const {
			return std::abs(x-q.x)<epsilon && std::abs(y-q.y)<epsilon && std::abs(z-q.z)<epsilon && std::abs(w-q.w)<epsilon;
		}*/

		Quaternion<T> invert()const {
			Quaternion<T> q;
			q.y *= -1;
			q.z *= -1;
			q.w *= -1;
			return q;
		}

		Vec3<T> rotate(const Vec3<T>& v)const {
			Quaternion<T> p(0, v.x, v.y, v.z);
			p = *this * p * invert();

			Vec3<T> vecteur3(p.y, p.z, p.w);
			return vecteur3;
		}
	};

	template<typename T>
	Vec2<T> operator* (const Vec2<T>& v, const Matrix2<T>& m) {
		Vec2<T> vOut;
		Matrix2<T> mTrans = m.trans();

		vOut.x = v * mTrans.vec1;
		vOut.y = v * mTrans.vec2;

		return vOut;
	}

	template<typename T>
	Vec3<T> operator* (const Vec3<T>& v, const Matrix3<T>& m) {
		Vec3<T> vOut;
		Matrix3<T> mTrans = m.trans();

		vOut.x = v * mTrans.vec1;
		vOut.y = v * mTrans.vec2;
		vOut.z = v * mTrans.vec3;

		return vOut;
	}

	template<typename T>
	Vec4<T> operator* (const Vec4<T>& v, const Matrix4<T>& m) {
		Vec4<T> vOut;
		Matrix4<T> mTrans = m.trans();

		vOut.x = v * mTrans.vec1;
		vOut.y = v * mTrans.vec2;
		vOut.z = v * mTrans.vec3;
		vOut.w = v * mTrans.vec4;

		return vOut;
	}


}



