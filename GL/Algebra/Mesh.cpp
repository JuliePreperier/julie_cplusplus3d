#include "Mesh.h"
#include "Algebra.h"
#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream> 


std::vector<algebra::Vec4<float>> Mesh::project(const std::vector<algebra::Vec3<float>>& points, int w, int h) const {
	algebra::Matrix4<float> projection;
	projection = projection.projectionMatrix(h, w, 90 * M_PI / 180, 10000, 0.1f);
	std::vector<algebra::Vec4<float>> projectedPoints;

	for (int i = 0; i < points.size(); i++) {
		algebra::Vec4<float> v4(points[i],1);
		v4 = v4 * projection;

		if (v4.w != 0) {
			v4.x /= v4.w;
			v4.y /= v4.w;
			v4.z /= v4.w;
		}

		projectedPoints.push_back(v4);
	}

	return projectedPoints;
}

std::vector<algebra::Vec3<float>> Mesh::view(const std::vector<algebra::Vec3<float>>& points, const algebra::Matrix4<float>& view) const
{
	std::vector<algebra::Vec3<float>> vOut;

	for (int i = 0; i < points.size(); i++) {
		algebra::Vec4<float> v4(points[i], 1);
		v4 = v4*view;

		vOut.push_back(algebra::Vec3<float> {v4.x, v4.y, v4.z});

	}
	return vOut;
}

bool Mesh::ExtractObj(const std::string & path)
{
	std::string line;
	std::ifstream ifs;
	// open file
	ifs.open(path);


	if (!ifs.is_open()) {
		std::cout << "Erreur � l'ouverture du fichier \n";
	}else
	{
		while (std::getline(ifs, line))
		{
			std::stringstream iss(line);
			char value;
			// iss enl�ve le caract�re de la ligne. -> en faisant iss >> value on prend le premier caract�re de la ligne et on l'enl�ve de la ligne.
			iss >> value;
			std::cout << line << '\n';
			switch (value) {
			case 'v': {
				// iss peut lire tout seul la ligne et d�tecter une valeur dans celle-ci.
				value = iss.peek();
				if (value == 'n') {
					// si les premi�re lettre sont vn ce ne sont pas des points mais des normals
					iss.ignore(256, ' ');
					algebra::Vec3<float> vec;
					iss >> vec.x;
					iss >> vec.y;
					iss >> vec.z;

					normals.push_back(vec);
				}
				else if (value == 't') {
					iss.ignore(256, ' ');
					algebra::Vec2<float> vec;
					iss >> vec.x; // uv.u
					iss >> vec.y; // uv.v

					UVs.push_back(vec);
				}
				else {
					
					algebra::Vec3<float> vec;
					iss >> vec.x;
					iss >> vec.y;
					iss >> vec.z;

					points.push_back(vec);
				}
				break;
			}
			case 'f':
				// f contient l'index du point(a), le UV (b) et l'index de la normal(c) -> a/b/c
				for (int i = 0; i < 3; i++) {
					std::string token;
					iss >> token;
					std::istringstream inneriss(token);
					std::string tok;

					std::getline(inneriss,tok,'/');
					float valIndicesPoint = atof(tok.c_str());

					std::getline(inneriss, tok, '/');
					float valIndicesUV = atof(tok.c_str());

					std::getline(inneriss, tok, '/');
					float valIndicesNormal = atof(tok.c_str());

					pointsIndex.push_back(valIndicesPoint-1);
					uvIndex.push_back(valIndicesUV-1);
					normalsIndex.push_back(valIndicesNormal-1);

				}
				break;
			}
		}
		ifs.close();
	}




	return false;
}
