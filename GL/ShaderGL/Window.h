#pragma once

#include <functional>
#include "SDL.h"
#include <GL/glew.h>
#include <chrono>
#include "Gui.h"

struct Window {
public:
	int windowWidth = 640;
	int windowHeight = 480;
	Gui gui;

	void InitWindow(int windowWidth, int windowHeight);

	void processWindow(std::function<bool(SDL_Event event)> processEvent, std::function<bool()> processImage);
	
	static void GLAPIENTRY ErrorMessageHandler(
		GLenum source,
		GLenum type,
		GLuint id,
		GLenum severity,
		GLsizei length,
		const GLchar* message,
		const void* userParam);

private:
	SDL_Window* sdlWindow;
	GLuint textureId = 0;
	int winWidth;
	int winHeight;

	std::string title;
};