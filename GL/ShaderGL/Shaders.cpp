#include "Shaders.h"
#include <fstream>
#include <iostream>

bool Shaders::loadShader(const std::string & path)
{
	std::string line;
	std::ifstream ifs;

	ifs.open(path);

	int result;
	int length;
	std::string errorMsg;

	if (!ifs.is_open()) {
		std::cout << "Erreur � l'ouverture du fichier shader \n";
		return false;
	}
	else
	{
		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		id = glCreateShader(type);
		const char* stringBuffer = str.c_str();

		glShaderSource(id, 1, &stringBuffer, NULL);
		glCompileShader(id);

		// If there is any other error
		glGetShaderiv(id, GL_COMPILE_STATUS, &result);
		if (result == GL_FALSE) {
			glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
			errorMsg.resize(length);
			glGetShaderInfoLog(id, length, &length, &errorMsg[0]);

			return false;
		}
		
		return true;
	}
	return true;
}
