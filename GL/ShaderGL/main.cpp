#define _USE_MATH_DEFINES

#include <GL/glew.h>
#include "Window.h"
#include "Shaders.h"
#include "Gui.h"

int main(int argc, char* argv[])
{
	Window win;
	win.InitWindow(win.windowWidth, win.windowHeight);

	unsigned int indices[] = {
		0, 1, 2
	};

	float points[] = {
		 0.0f,  0.5f, 0.0f,
		 0.5f, -0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f
	};

	// Tableau en RGB
	float colors[] = {
		1.0f, 0.f, 0.f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f
	};



	/* ERROR MESSAGE */
	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(
		Window::ErrorMessageHandler,
		nullptr);

	/* BUFFERS */

	// Index Buffer
	unsigned int indexBufferObject = 0;
	glGenBuffers(1, &indexBufferObject);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
	glBufferData(
		GL_ELEMENT_ARRAY_BUFFER,
		3 * sizeof(unsigned int),
		indices,
		GL_STATIC_DRAW);

	// Vertex Buffer
	unsigned int vertexBufferObject = 0;
	glGenBuffers(1, &vertexBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER,
		vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER,
		9 * sizeof(float),
		points,
		GL_STATIC_DRAW);

	// Color Buffer
	unsigned int colorBufferObject = 0;
	glGenBuffers(1, &colorBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, colorBufferObject);
	glBufferData(GL_ARRAY_BUFFER,
		9 * sizeof(float),
		colors,
		GL_STATIC_DRAW);

	/* SHADERS */

	GLuint vertexAttribObject = 0;
	glGenVertexArrays(1, &vertexAttribObject);
	glBindVertexArray(vertexAttribObject);
	
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	
	glBindBuffer(GL_ARRAY_BUFFER, colorBufferObject);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	Shaders vertexShader(GL_VERTEX_SHADER);
	Shaders fragmentShader(GL_FRAGMENT_SHADER);

	if (!vertexShader.loadShader(".\\vertexShader.txt") ||
		!fragmentShader.loadShader(".\\fragmentShader.txt")) {
		exit(-1);
	}

	// Create program that contain the shaders
	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, fragmentShader.GetId());
	glAttachShader(shaderProgram, vertexShader.GetId());
	glLinkProgram(shaderProgram);
	glUseProgram(shaderProgram);

	win.processWindow([&win](SDL_Event event)
	{
		switch (event.type) {
	
		case SDL_KEYDOWN:
			if (event.key.keysym.sym == SDLK_ESCAPE) {
				return false;
			}
			break;

		case SDL_QUIT:
			return false;
		}
		return true;
	},
		[&win, &indexBufferObject, &colorBufferObject, &vertexBufferObject]()
	{
		float colors[] = {
			win.gui.colorTop.x,		win.gui.colorTop.y,		win.gui.colorTop.z,
			win.gui.colorRight.x,	win.gui.colorRight.y,	win.gui.colorRight.z,
			win.gui.colorLeft.x,	win.gui.colorLeft.y,	win.gui.colorLeft.z
		};

		float points[] = {
			 win.gui.positionTop.x,		win.gui.positionTop.y,		0.0f,
			 win.gui.positionRight.x,	win.gui.positionRight.y,	0.0f,
			 win.gui.positionLeft.x,	win.gui.positionLeft.y,		0.0f
		};

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
		
		glBindBuffer(GL_ARRAY_BUFFER, colorBufferObject);
		glBufferData(GL_ARRAY_BUFFER,
			9 * sizeof(float),
			colors,
			GL_STATIC_DRAW);
		
		glBindBuffer(GL_ARRAY_BUFFER,
			vertexBufferObject);
		glBufferData(GL_ARRAY_BUFFER,
			9 * sizeof(float),
			points,
			GL_STATIC_DRAW);


		glDrawElements(
			GL_TRIANGLES,  // What do we draw.
			3,               // 3 points.
			GL_UNSIGNED_INT, // This HAS to be unsigned!
			nullptr);        // Delta to the beginning.

		return true;
	});

	return 0;
}