#pragma once
#include <GL/glew.h>
#include <string>

struct Shaders {
public:
	// Constructor
	Shaders(GLuint shaderType) {
		type = shaderType;
	}
	bool loadShader(const std::string &path);
	GLuint GetId() const {
		return id;
	}

private:
	GLuint type;
	GLuint id;
};