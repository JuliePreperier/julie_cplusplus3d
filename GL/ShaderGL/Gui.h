#pragma once
#include "SDL.h"
#include <imgui.h>

struct Gui {
public:

	Gui(){
		ResetColor();
		ResetPosition();
	}

	void GuiFrame(SDL_Window* sdlWin); // Define imGui Frame
	void InitGui(SDL_Window* sdlWin, SDL_GLContext context);
	void RenderGui(SDL_Window* sdlWin);
	void ResetColor();
	void ResetPosition();

	ImVec4 colorTop;
	ImVec4 colorRight;
	ImVec4 colorLeft;

	ImVec2 positionTop;
	ImVec2 positionRight;
	ImVec2 positionLeft;

private:
	void InitFrame(SDL_Window* sdlWin);
};