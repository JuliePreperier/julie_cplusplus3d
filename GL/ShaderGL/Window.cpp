#include <functional>
#include "Window.h"
#include <GL/glew.h>
#include <string>
#include <chrono>
#include <iostream>
#include <imgui.h>
#include "../ImGuiImpl/imgui_impl_sdl.h"
#include "../ImGuiImpl/imgui_impl_opengl3.h"


void Window::InitWindow(int windowWidth, int windowHeight) {
	winHeight = windowHeight;
	winWidth = windowWidth;

	SDL_Init(SDL_INIT_VIDEO);

	sdlWindow = SDL_CreateWindow
	("Will be erased anyway", 10, 25, winWidth, winHeight, SDL_WINDOW_OPENGL);

	auto sdlGlContext =
		SDL_GL_CreateContext(sdlWindow);

	if (GLEW_OK != glewInit()) {
		std::cout << "Glew not correctly init.";
		// kill app
		exit(-1);
	}

	SDL_GL_SetAttribute(
		SDL_GL_CONTEXT_PROFILE_MASK,
		SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_GL_SetAttribute(
		SDL_GL_CONTEXT_MAJOR_VERSION, 4);

	SDL_GL_SetAttribute(
		SDL_GL_CONTEXT_MINOR_VERSION, 3);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	glGenTextures(1, &textureId);

	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexParameteri(GL_TEXTURE_2D,
		GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,
		GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	gui.InitGui(sdlWindow, sdlGlContext);


}




void Window::processWindow(std::function<bool(SDL_Event event)> processEvent, std::function<bool()> processImage) {

	bool loop = true;

	int frame = 0;
	auto startTime = std::chrono::system_clock::now();
	std::chrono::duration<float> deltaTime;
	SDL_GL_SetSwapInterval(0);

	while (loop) {
		/* -- EVENT MANAGER --*/
		SDL_Event lastEvent{};
		SDL_Event event;

		if (SDL_PollEvent(&event)) {
			ImGui_ImplSDL2_ProcessEvent(&event);
			if (event.type != lastEvent.type) {
				if (!processEvent(event)) {
					loop = false;
				}
			}
			lastEvent = event;
		}

		// Call GuiFrame method
		gui.GuiFrame(sdlWindow); 


		/* -- TITLE + FPS CALCULATION --*/
		
		if ((startTime - std::chrono::system_clock::now()).count() > 1.0f) {
			startTime = std::chrono::system_clock::now();
			frame = 0;
		}

		deltaTime = std::chrono::system_clock::now() - startTime;
		frame++;

		float fps = frame / deltaTime.count();

		title = "Titre de la fenetre - " + std::to_string((int)fps) +"fps";
		SDL_SetWindowTitle(sdlWindow, title.c_str());

		/* CLEAR GL*/

		glClear(
			GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		processImage();

		glFlush();

		// Render ImGui
		gui.RenderGui(sdlWindow);
	}

}



void GLAPIENTRY Window::ErrorMessageHandler(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar * message, const void * userParam)
{
	std::cout << message  << std::endl;
}