cmake_minimum_required(VERSION 3.10)
project(SoftwareGL)
find_package(SDL2 CONFIG REQUIRED)
find_package(GLEW REQUIRED)
find_package(imgui CONFIG REQUIRED)

add_library(SoftwareGL STATIC
${PROJECT_SOURCE_DIR}/Algebra/Algebra.h
${PROJECT_SOURCE_DIR}/Algebra/Camera.h
${PROJECT_SOURCE_DIR}/Algebra/Camera.cpp
${PROJECT_SOURCE_DIR}/Algebra/Image.h
${PROJECT_SOURCE_DIR}/Algebra/Image.cpp
${PROJECT_SOURCE_DIR}/Algebra/Mesh.h
${PROJECT_SOURCE_DIR}/Algebra/Mesh.cpp
${PROJECT_SOURCE_DIR}/Algebra/Tga.h
${PROJECT_SOURCE_DIR}/Algebra/Tga.cpp
)

target_link_libraries(SoftwareGL PRIVATE
	SDL2::SDL2
	GLEW::GLEW
)

add_library(ImguiImpl STATIC
${PROJECT_SOURCE_DIR}/ImGuiImpl/imgui_impl_sdl.h
${PROJECT_SOURCE_DIR}/ImGuiImpl/imgui_impl_sdl.cpp
${PROJECT_SOURCE_DIR}/ImGuiImpl/imgui_impl_opengl3.h
${PROJECT_SOURCE_DIR}/ImGuiImpl/imgui_impl_opengl3.cpp
)

target_link_libraries(ImguiImpl PRIVATE
	SDL2::SDL2
	GLEW::GLEW
)


add_executable(Game
${PROJECT_SOURCE_DIR}/Algebra/Main.cpp
${PROJECT_SOURCE_DIR}/Algebra/Window.h
${PROJECT_SOURCE_DIR}/Algebra/Window.cpp
)

target_link_libraries(Game PRIVATE
	SoftwareGL
	SDL2::SDL2
	SDL2::SDL2main
	GLEW::GLEW
)

add_executable(ShaderGL
${PROJECT_SOURCE_DIR}/ShaderGL/main.cpp
${PROJECT_SOURCE_DIR}/ShaderGL/Window.h
${PROJECT_SOURCE_DIR}/ShaderGL/Window.cpp
${PROJECT_SOURCE_DIR}/ShaderGL/Shaders.h
${PROJECT_SOURCE_DIR}/ShaderGL/Shaders.cpp
${PROJECT_SOURCE_DIR}/ShaderGL/Gui.h
${PROJECT_SOURCE_DIR}/ShaderGL/Gui.cpp
)

target_link_libraries(ShaderGL PRIVATE
	SoftwareGL
	ImguiImpl
	SDL2::SDL2
	SDL2::SDL2main
	GLEW::GLEW
	imgui::imgui
)

